unit control;

interface

uses
  crt,
  values,
  structure;

procedure input_path;
procedure boop(code: byte);
procedure input(field_type: byte; var tmp: ShortString; var error: boolean);

implementation

procedure boop(code: byte);
begin
  case code of
    0:
    begin
      Sound(700);
      Delay(200);
      NoSound;
    end;
    1:
    begin
      Sound(400);
      Delay(200);
      NoSound;
    end;
  end;
  NoSound;
end;

procedure input(field_type: byte; var tmp: ShortString; var error: boolean);
var
  loop: boolean;
  ch: char;
begin
  loop := False;
  error := False;
  GotoXY(1, 1);
  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_BACKGROUND);
  ClrEol;
  GotoXY(1, 1);
  cursoron;
  Write(tmp);
  case field_type of
    1:
    begin
      repeat
        ch := ReadKey;
        case ch of
          #0:
          begin
            ReadKey;
          end;
          #32..#125, #192..#255:
          begin
            if (Length(tmp) < 74) then
            begin
              Write(ch);
              tmp += ch;
            end
            else
              boop(1);
          end;
          #8:
          begin
            if (Length(tmp) > 0) then
            begin
              SetLength(tmp, Length(tmp) - 1);
              GotoXY(WhereX - 1, WhereY);
              Write(#32);
              GotoXY(WhereX - 1, WhereY);
            end
            else
              boop(1);
          end;
          #127:
          begin
            tmp := '';
            GotoXY(1, 1);
            ClrEol;
          end;
          #13:
          begin
            loop := True;
          end;
        end;
      until loop;
    end;
    2:
    begin
      repeat
        ch := ReadKey;
        case ch of
          #0:
          begin
            ReadKey;
          end;
          #48..#57:
          begin
            if (Length(tmp) < 9) then
            begin
              Write(ch);
              tmp += ch;
            end
            else
              boop(1);
          end;
          #8:
          begin
            if (Length(tmp) > 0) then
            begin
              SetLength(tmp, Length(tmp) - 1);
              GotoXY(WhereX - 1, WhereY);
              Write(#32);
              GotoXY(WhereX - 1, WhereY);
            end
            else
              boop(1);
          end;
          #127:
          begin
            tmp := '';
            GotoXY(1, 1);
            ClrEol;
          end;
          #13:
          begin
            loop := True;
          end;
        end;
      until loop;
    end;
    3:
    begin
      repeat
        ch := ReadKey;
        case ch of
          #0:
          begin
            ReadKey;
          end;
          #48..#53:
          begin
            if (Length(tmp) < 1) then
            begin
              Write(ch);
              tmp += ch;
            end
            else
              boop(1);
          end;
          #8:
          begin
            if (Length(tmp) > 0) then
            begin
              SetLength(tmp, Length(tmp) - 1);
              GotoXY(WhereX - 1, WhereY);
              Write(#32);
              GotoXY(WhereX - 1, WhereY);
            end
            else
              boop(1);
          end;
          #127:
          begin
            tmp := '';
            GotoXY(1, 1);
            ClrEol;
          end;
          #13:
          begin
            loop := True;
          end;
        end;
      until loop;
    end;
  end;
  cursoroff;
  GotoXY(1, 1);
  if (not error) then
  begin
    TextColor(WINDOW_FORGEGROUND);
    TextBackground(WINDOW_BACKGROUND);
    ClrEol;
  end;
end;

procedure input_path;
var
  loop: boolean;
  ch: char;
begin
  loop := False;
  cursoron;
  Write(path);
  repeat
    ch := ReadKey;
    case ch of
      #0:
      begin
        ReadKey;
      end;
      #32..#125, #192..#255:
      begin
        if (Length(path) < 500) then
        begin
          Write(ch);
          path += ch;
        end
        else
          boop(1);
      end;
      #8:
      begin
        if (Length(path) > 0) then
        begin
          SetLength(path, Length(path) - 1);
          ClrScr;
          Write(path);
        end
        else
          boop(1);
      end;
      #127:
      begin
        path := '';
        GotoXY(1, 1);
        ClrScr;
      end;
      #27:
      begin
        path := #1;
        loop := True;
      end;
      #13:
      begin
        loop := True;
      end;
    end;
  until loop; 
  cursoroff;
end;

end.
