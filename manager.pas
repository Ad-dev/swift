unit manager;

interface

uses
  crt,
  control,
  printer,
  structure;

procedure saver;
procedure sorter;
procedure outset;
procedure searcher;
procedure edit_cell;
procedure disappear;
procedure add_record;
procedure crash_table;
procedure elimination;
procedure annihilation;
procedure confirm_sort;
procedure add_sub_record;
function loader: boolean;
function edit_path: boolean;
procedure dependency_code_optimization;
procedure swap_position(offset_vector: byte);
procedure swap_view_position(offset_vector: byte);

implementation

procedure add_record;
var
  node: line_tab_2;
begin
  new(node);
  new(node^.developer);
  node^.developer^ := '';
  node^.description := '';
  node^.e_mail := '';
  Inc(max_code);
  node^.num_code := max_code;

  if (pointer_to_tab_2 = nil) then
  begin
    node^.upper := nil;
    node^.lower := nil;

    pointer_to_tab_2 := node;

    current_position_x := 1;
    current_position_y := 1;
    current_page := 1;
  end
  else
  begin
    node^.upper := pointer_to_tab_2;
    node^.lower := pointer_to_tab_2^.lower;
    if (pointer_to_tab_2^.lower <> nil) then
      pointer_to_tab_2^.lower^.upper := node;
    pointer_to_tab_2^.lower := node;
    swap_position(2);
  end;
end;

procedure add_sub_record;
var
  node: line_tab_1;
begin
  new(node);
  node^.program_name := '';
  node^.size := 0;
  node^.cost := 0;
  node^.rating := '0';
  node^.developer := pointer_to_tab_2^.developer;
  node^.dependency_code := pointer_to_tab_2^.num_code;

  if (pointer_to_tab_1 = nil) then
  begin
    node^.upper := nil;
    node^.lower := nil;

    pointer_to_tab_1 := node;

    current_position_x := 1;
    current_position_y := 1;
    current_page := 1;
  end
  else
  begin
    while (pointer_to_tab_1^.lower <> nil) do
      pointer_to_tab_1 := pointer_to_tab_1^.lower;
    node^.upper := pointer_to_tab_1;
    node^.lower := nil;
    pointer_to_tab_1^.lower := node;
  end;
end;

procedure swap_position(offset_vector: byte);
begin
  show_position(False, False);
  case offset_vector of
    1:
    begin
      if (current_table = 0) then
      begin
        if (current_position_x < 5) then
          current_position_x := current_position_x + 1
        else
          boop(1);
      end
      else
      begin
        if (current_position_x < 3) then
          current_position_x := current_position_x + 1
        else
          boop(1);
      end;
    end;
    2:
    begin
      if (current_table = 0) then
        if (pointer_to_tab_1^.lower <> nil) then
        begin
          current_position_y := current_position_y + 1;
          pointer_to_tab_1 := pointer_to_tab_1^.lower;
          if (current_position_y > 23) then
          begin
            current_position_y := 1;
            current_page := current_page + 1;
            print_records;
          end;
        end
        else
          boop(1)
      else
      if (pointer_to_tab_2^.lower <> nil) then
      begin
        current_position_y := current_position_y + 1;
        pointer_to_tab_2 := pointer_to_tab_2^.lower;
        if (current_position_y > 23) then
        begin
          current_position_y := 1;
          current_page := current_page + 1;
          print_records;
        end;
      end
      else
        boop(1);
    end;
    3:
    begin
      if (current_position_x > 1) then
        current_position_x := current_position_x - 1
      else
        boop(1);
    end;
    4:
    begin
      if (current_table = 0) then
        if (pointer_to_tab_1^.upper <> nil) then
        begin
          current_position_y := current_position_y - 1;
          pointer_to_tab_1 := pointer_to_tab_1^.upper;
          if (current_position_y < 1) then
          begin
            current_position_y := 23;
            current_page := current_page - 1;
            print_records;
          end;
        end
        else
          boop(1)
      else
      if (pointer_to_tab_2^.upper <> nil) then
      begin
        current_position_y := current_position_y - 1;
        pointer_to_tab_2 := pointer_to_tab_2^.upper;
        if (current_position_y < 1) then
        begin
          current_position_y := 23;
          current_page := current_page - 1;
          print_records;
        end;
      end
      else
        boop(1);
    end;
  end;
  show_position(True, False);
end;

procedure edit_cell;
var
  old: cardinal;
  error: boolean;
  err: byte;
  olds, tmp: ShortString;
begin
  tmp := '';
  error := False;
  show_position(True, True);
  if (current_table = 0) then
  begin
    case current_position_x of
      1:
      begin
        tmp := pointer_to_tab_1^.program_name;
        input(1, tmp, error);
        if (not error) then
          pointer_to_tab_1^.program_name := tmp
        else
        begin
          boop(0);
          error_message;
        end;
      end;
      2:
      begin
        if (pointer_to_tab_1^.size <> 0) then
          str(pointer_to_tab_1^.size, tmp)
        else
          tmp := '';
        old := pointer_to_tab_1^.size;
        input(2, tmp, error);
        val(tmp, pointer_to_tab_1^.size, err);
        if (err <> 0) then
        begin
          pointer_to_tab_1^.size := old;
          boop(0);
          error_message;
        end;
      end;
      3:
      begin
        if (pointer_to_tab_1^.cost <> 0) then
          str(pointer_to_tab_1^.cost, tmp)
        else
          tmp := '';
        old := pointer_to_tab_1^.cost;
        input(2, tmp, error);
        val(tmp, pointer_to_tab_1^.cost, err);
        if (err <> 0) then
        begin
          old := pointer_to_tab_1^.cost;
          boop(0);
          error_message;
        end;
      end;
      4:
      begin
        tmp := pointer_to_tab_1^.developer^;
        input(1, tmp, error);
        if (not error) then
        begin
          pointer_to_tab_1^.developer^ := tmp;
          print_records;
        end
        else
        begin
          boop(0);
          error_message;
        end;
      end;
      5:
      begin
        olds := pointer_to_tab_1^.rating;
        tmp := '';
        input(3, tmp, error);
        if (not error) then
          if ((tmp[1] = #48) or (tmp[1] = #49) or (tmp[1] = #50) or
            (tmp[1] = #51) or (tmp[1] = #52) or (tmp[1] = #53)) then
            pointer_to_tab_1^.rating := tmp[1]
          else
            pointer_to_tab_1^.rating := olds[1]
        else
        begin
          pointer_to_tab_1^.rating := olds[1];
          boop(0);
          error_message;
        end;
      end;
    end;
  end
  else
  begin
    case current_position_x of
      1:
      begin
        tmp := pointer_to_tab_2^.developer^;
        input(1, tmp, error);
        if (not error) then
          pointer_to_tab_2^.developer^ := tmp
        else
        begin
          boop(0);
          error_message;
        end;
      end;
      2:
      begin
        tmp := pointer_to_tab_2^.description;
        input(1, tmp, error);
        if (not error) then
          pointer_to_tab_2^.description := tmp
        else
        begin
          boop(0);
          error_message;
        end;
      end;
      3:
      begin
        tmp := pointer_to_tab_2^.e_mail;
        input(1, tmp, error);
        if (not error) then
          pointer_to_tab_2^.e_mail := tmp
        else
        begin
          boop(0);
          error_message;
        end;
      end;
    end;
  end;
  show_position(True, False);
  if (view_mode) then
    view_message;
end;

procedure annihilation;
var
  node_upper, node_lower: line_tab_2;
  n: QWord;
  break: boolean;
begin
  n := pointer_to_tab_2^.num_code;
  node_upper := pointer_to_tab_2^.upper;
  node_lower := pointer_to_tab_2^.lower;
  if ((pointer_to_tab_2^.upper = nil) and (pointer_to_tab_2^.lower = nil)) then
  begin
    pointer_to_tab_2^.developer^ := '';
    pointer_to_tab_2^.description := '';
    pointer_to_tab_2^.e_mail := '';
    print_records;
  end
  else
  begin
    if (node_upper <> nil) then
    begin
      node_upper^.lower := node_lower;
    end;
    if (node_lower <> nil) then
      node_lower^.upper := node_upper;
    if (node_upper = nil) then
    begin
      list_peak_2 := node_lower;
    end;
    dispose(pointer_to_tab_2^.developer);
    dispose(pointer_to_tab_2);
    pointer_to_tab_2 := list_peak_2;
  end;

  break := False;
  pointer_to_tab_1 := list_peak_1;
  while (pointer_to_tab_1 <> nil) do
  begin
    if ((pointer_to_tab_1^.dependency_code = n) and (not break)) then
    begin
      if ((pointer_to_tab_1^.upper = nil) and (pointer_to_tab_1^.lower = nil)) then
      begin
        pointer_to_tab_1^.program_name := '';
        pointer_to_tab_1^.size := 0;
        pointer_to_tab_1^.cost := 0;
        pointer_to_tab_1^.developer := pointer_to_tab_2^.developer;
        pointer_to_tab_1^.rating := '0';
        break := True;
      end
      else
      begin
        if (pointer_to_tab_1^.upper <> nil) then
        begin
          pointer_to_tab_1^.upper^.lower := pointer_to_tab_1^.lower;
        end;
        if (pointer_to_tab_1^.lower <> nil) then
          pointer_to_tab_1^.lower^.upper := pointer_to_tab_1^.upper;
        if (pointer_to_tab_1^.upper = nil) then
          list_peak_1 := pointer_to_tab_1^.lower;
        dispose(pointer_to_tab_1);
        pointer_to_tab_1 := list_peak_1;
      end;
    end
    else
      pointer_to_tab_1 := pointer_to_tab_1^.lower;
  end;
  current_table := 1;
  current_position_x := 1;
  current_position_y := 1;
  current_page := 1;
  dependency_code_optimization;
  print_records;
end;

procedure elimination;
var
  node: line_tab_1;
begin
  if ((pointer_to_tab_1^.upper = nil) and (pointer_to_tab_1^.lower = nil)) then
  begin
    pointer_to_tab_1^.program_name := '';
    pointer_to_tab_1^.size := 0;
    pointer_to_tab_1^.cost := 0;
    pointer_to_tab_1^.rating := '0';
    print_records;
  end
  else
  begin
    node := pointer_to_tab_1;
    if (pointer_to_tab_1^.upper <> nil) then
    begin
      pointer_to_tab_1^.upper^.lower := pointer_to_tab_1^.lower;
    end;
    if (pointer_to_tab_1^.lower <> nil) then
      pointer_to_tab_1^.lower^.upper := pointer_to_tab_1^.upper;
    if (node^.upper = nil) then
    begin
      list_peak_1 := pointer_to_tab_1^.lower;
      pointer_to_tab_1 := list_peak_1;
    end
    else
    begin
      swap_position(4);
    end;
    dispose(node);
  end;
end;

procedure disappear;
begin
  view_node := view_head;
  while (view_node <> nil) do
  begin
    view_head := view_node^.lower;
    dispose(view_node);
    view_node := view_head;
  end;
  view_mode := False;
  search := False;
  sort := False;
  current_position_x := 1;
  current_position_y := 1;
  current_page := 1;
  current_table := 0;
  pointer_to_tab_1 := list_peak_1;
  print_records;
end;

procedure outset;
begin
  pointer_to_tab_1 := nil;
  pointer_to_tab_2 := nil;
  view_mode := False;
  search := False;
  sort := False;
  current_position_x := 0;
  current_position_y := 0;
  current_table := 1;
  current_page := 0;
  max_code := 0;
  add_record;
  add_sub_record;

  view_node := nil;
  view_head := nil;
  list_peak_2 := pointer_to_tab_2;
  list_peak_1 := pointer_to_tab_1;

  print_records;
end;

procedure dependency_code_optimization;
var
  n: QWord;
  old_1: line_tab_1;
  old_2: line_tab_2;
begin
  old_1 := pointer_to_tab_1;
  old_2 := pointer_to_tab_2;
  n := 1;
  pointer_to_tab_2 := list_peak_2;

  while (pointer_to_tab_2^.lower <> nil) do
  begin
    pointer_to_tab_1 := list_peak_1;
    while (pointer_to_tab_1^.lower <> nil) do
    begin
      if (pointer_to_tab_1^.dependency_code = pointer_to_tab_2^.num_code) then
        pointer_to_tab_1^.dependency_code := n;
      pointer_to_tab_1 := pointer_to_tab_1^.lower;
    end;
    if (pointer_to_tab_1^.dependency_code = pointer_to_tab_2^.num_code) then
      pointer_to_tab_1^.dependency_code := n;
    pointer_to_tab_2^.num_code := n;
    Inc(n);
    pointer_to_tab_2 := pointer_to_tab_2^.lower;
  end;
  pointer_to_tab_1 := list_peak_1;
  while (pointer_to_tab_1^.lower <> nil) do
  begin
    if (pointer_to_tab_1^.dependency_code = pointer_to_tab_2^.num_code) then
      pointer_to_tab_1^.dependency_code := n;
    pointer_to_tab_1 := pointer_to_tab_1^.lower;
  end;
  if (pointer_to_tab_1^.dependency_code = pointer_to_tab_2^.num_code) then
    pointer_to_tab_1^.dependency_code := n;
  pointer_to_tab_2^.num_code := n;
  max_code := n;

  pointer_to_tab_1 := old_1;
  pointer_to_tab_2 := old_2;
end;

procedure searcher;
begin
  view_node := nil;
  view_head := nil;
  pointer_to_tab_1 := list_peak_1;
  while (pointer_to_tab_1^.lower <> nil) do
  begin
    if (pointer_to_tab_1^.dependency_code = pointer_to_tab_2^.num_code) then
    begin
      if (view_node = nil) then
      begin
        new(view_node);
        view_head := view_node;
        view_node^.upper := nil;
        view_node^.to_firs_tab := pointer_to_tab_1;
        view_node^.lower := nil;
      end
      else
      begin
        new(view_node^.lower);
        view_node^.lower^.upper := view_node;
        view_node := view_node^.lower;
        view_node^.to_firs_tab := pointer_to_tab_1;
        view_node^.lower := nil;
      end;
      pointer_to_tab_1 := pointer_to_tab_1^.lower;
    end
    else
      pointer_to_tab_1 := pointer_to_tab_1^.lower;
  end;
  if (pointer_to_tab_1^.dependency_code = pointer_to_tab_2^.num_code) then
    if (view_node = nil) then
    begin
      new(view_node);
      view_head := view_node;
      view_node^.upper := nil;
      view_node^.to_firs_tab := pointer_to_tab_1;
      view_node^.lower := nil;
    end
    else
    begin
      new(view_node^.lower);
      view_node^.lower^.upper := view_node;
      view_node := view_node^.lower;
      view_node^.to_firs_tab := pointer_to_tab_1;
      view_node^.lower := nil;
    end;
  if (view_head <> nil) then
  begin
    view_mode := True;
    search := True;
    current_position_x := 1;
    current_position_y := 1;
    current_page := 1;
    current_table := 0;
    pointer_to_tab_1 := list_peak_1;
    view_node := view_head;
    print_records;
  end
  else
  begin
    boop(1);
    error_message;
  end;
end;

procedure swap_view_position(offset_vector: byte);
begin
  show_position(False, False);
  case offset_vector of
    1:
    begin
      if (current_position_x < 5) then
        current_position_x := current_position_x + 1
      else
        boop(1);
    end;
    2:
    begin
      if (view_node^.lower <> nil) then
      begin
        current_position_y := current_position_y + 1;
        view_node := view_node^.lower;
        pointer_to_tab_1 := view_node^.to_firs_tab;
        if (current_position_y > 23) then
        begin
          current_position_y := 1;
          current_page := current_page + 1;
          print_records;
        end;
      end
      else
        boop(1);
    end;
    3:
    begin
      if (current_position_x > 1) then
        current_position_x := current_position_x - 1
      else
        boop(1);
    end;
    4:
    begin
      if (view_node^.upper <> nil) then
      begin
        current_position_y := current_position_y - 1;
        view_node := view_node^.upper;
        pointer_to_tab_1 := view_node^.to_firs_tab;
        if (current_position_y < 1) then
        begin
          current_position_y := 23;
          current_page := current_page - 1;
          print_records;
        end;
      end
      else
        boop(1);
    end;
  end;
  show_position(True, False);
end;

procedure sorter;
var
  s: array of line_tab_1;
  x: line_tab_1;
  i, j: QWord;
begin
  view_node := nil;
  SetLength(s, 0);
  pointer_to_tab_1 := list_peak_1;
  if ((pointer_to_tab_1^.upper <> nil) or (pointer_to_tab_1^.lower <> nil)) then
  begin
    while (pointer_to_tab_1 <> nil) do
    begin
      SetLength(s, Length(s) + 1);
      s[Length(s) - 1] := pointer_to_tab_1;
      pointer_to_tab_1 := pointer_to_tab_1^.lower;
    end;

    for i := 0 to Length(s) - 2 do
      for j := 0 to Length(s) - 2 do
        if (s[j]^.program_name > s[j + 1]^.program_name) then
        begin
          x := s[j];
          s[j] := s[j + 1];
          s[j + 1] := x;
        end;
    for i := 0 to Length(s) - 1 do
    begin
      if (view_node = nil) then
      begin
        new(view_node);
        view_head := view_node;
        view_node^.upper := nil;
        view_node^.to_firs_tab := s[i];
        view_node^.lower := nil;
      end
      else
      begin
        new(view_node^.lower);
        view_node^.lower^.upper := view_node;
        view_node := view_node^.lower;
        view_node^.to_firs_tab := s[i];
        view_node^.lower := nil;
      end;
    end;
    view_mode := True;
    sort := True;
    current_position_x := 1;
    current_position_y := 1;
    current_page := 1;
    current_table := 0;
    pointer_to_tab_1 := list_peak_1;
    view_node := view_head;
    print_records;
  end
  else
  begin
    boop(0);
    error_message;
  end;
end;

procedure crash_table;
begin
  if (view_head <> nil) then
    disappear;
  pointer_to_tab_1 := list_peak_1;
  while (pointer_to_tab_1 <> nil) do
  begin
    list_peak_1 := pointer_to_tab_1^.lower;
    dispose(pointer_to_tab_1);
    pointer_to_tab_1 := list_peak_1;
  end;
  pointer_to_tab_2 := list_peak_2;
  while (pointer_to_tab_2 <> nil) do
  begin
    list_peak_2 := pointer_to_tab_2^.lower;
    dispose(pointer_to_tab_2^.developer);
    dispose(pointer_to_tab_2);
    pointer_to_tab_2 := list_peak_2;
    view_head := nil;
    view_node := nil;
    pointer_to_tab_1 := nil;
    pointer_to_tab_2 := nil;
    list_peak_1 := nil;
    list_peak_2 := nil;
  end;
end;

procedure confirm_sort;
var
  node: line_tab_1;
  First: boolean;
begin
  First := True;
  view_node := view_head;
  node := nil;
  while (view_node <> nil) do
  begin
    pointer_to_tab_1 := view_node^.to_firs_tab;
    if (First) then
    begin
      First := False;
      list_peak_1 := view_node^.to_firs_tab;
    end;
    pointer_to_tab_1^.upper := node;
    node := view_node^.to_firs_tab;
    if (pointer_to_tab_1^.upper <> nil) then
      pointer_to_tab_1^.upper^.lower := pointer_to_tab_1;
    pointer_to_tab_1^.lower := nil;

    pointer_to_tab_1 := pointer_to_tab_1^.lower;
    view_node := view_node^.lower;
  end;
  disappear;
end;

function loader: boolean;
var
  f: file;
  snode_1: save_record_1;
  snode_2: save_record_2;
  node_1: line_tab_1;
  node_2: line_tab_2;
  i: QWord;
begin
  loader := True;
  {$I-}
  Assign(f, path);
  Reset(f, 1);
  {$I+}
  if ioresult = 0 then
  begin
  {$I-}
    BlockRead(f, max_code, SizeOf(max_code));
    for i := 1 to max_code do
    begin
      BlockRead(f, snode_2, SizeOf(snode_2));
      new(node_2);
      new(node_2^.developer);
      node_2^.developer^ := snode_2.developer;
      node_2^.description := snode_2.description;
      node_2^.e_mail := snode_2.e_mail;
      node_2^.num_code := i;
      if (pointer_to_tab_2 = nil) then
      begin
        node_2^.upper := nil;
        node_2^.lower := nil;
        pointer_to_tab_2 := node_2;
        list_peak_2 := pointer_to_tab_2;
      end
      else
      begin
        node_2^.upper := pointer_to_tab_2;
        node_2^.lower := pointer_to_tab_2^.lower;
        if (pointer_to_tab_2^.lower <> nil) then
          pointer_to_tab_2^.lower^.upper := node_2;
        pointer_to_tab_2^.lower := node_2;
        pointer_to_tab_2 := pointer_to_tab_2^.lower;
      end;
    end;
    while (not EOF(f)) do
    begin
      BlockRead(f, snode_1, SizeOf(snode_1));
      new(node_1);
      node_1^.program_name := snode_1.program_name;
      node_1^.size := snode_1.size;
      node_1^.cost := snode_1.cost;
      node_1^.rating := snode_1.rating;
      node_1^.dependency_code := snode_1.dependency_code;
      pointer_to_tab_2 := list_peak_2;
      while (node_1^.dependency_code <> pointer_to_tab_2^.num_code) do
        pointer_to_tab_2 := pointer_to_tab_2^.lower;
      node_1^.developer := pointer_to_tab_2^.developer;
      if (pointer_to_tab_1 = nil) then
      begin
        node_1^.upper := nil;
        node_1^.lower := nil;
        pointer_to_tab_1 := node_1;
        list_peak_1 := pointer_to_tab_1;
      end
      else
      begin
        while (pointer_to_tab_1^.lower <> nil) do
          pointer_to_tab_1 := pointer_to_tab_1^.lower;
        node_1^.upper := pointer_to_tab_1;
        node_1^.lower := nil;
        pointer_to_tab_1^.lower := node_1;
      end;
    end;
  end;
  Close(f);
  {$I+}
  if ioresult <> 0 then
  begin
    loader := False;
    error_message;
  end
  else
  begin
    view_mode := False;
    search := False;
    sort := False;
    pointer_to_tab_1 := list_peak_1;
    pointer_to_tab_2 := list_peak_2;
    current_position_x := 1;
    current_position_y := 1;
    current_table := 1;
    current_page := 1;
    print_records;
  end;
end;

procedure saver;
var
  f: file;
  snode_1: save_record_1;
  snode_2: save_record_2;
  i: QWord;
begin
  {$I-}
  Assign(f, path);
  Rewrite(f, 1);
  BlockWrite(f, max_code, SizeOf(max_code));

  pointer_to_tab_2 := list_peak_2;
  for i := 1 to max_code do
  begin
    snode_2.developer := pointer_to_tab_2^.developer^;
    snode_2.description := pointer_to_tab_2^.description;
    snode_2.e_mail := pointer_to_tab_2^.e_mail;
    BlockWrite(f, snode_2, SizeOf(snode_2));
    if (pointer_to_tab_2^.lower <> nil) then
      pointer_to_tab_2 := pointer_to_tab_2^.lower;
  end;

  pointer_to_tab_1 := list_peak_1;
  while (pointer_to_tab_1 <> nil) do
  begin
    snode_1.program_name := pointer_to_tab_1^.program_name;
    snode_1.size := pointer_to_tab_1^.size;
    snode_1.cost := pointer_to_tab_1^.cost;
    snode_1.rating := pointer_to_tab_1^.rating;
    snode_1.dependency_code := pointer_to_tab_1^.dependency_code;
    BlockWrite(f, snode_1, SizeOf(snode_1));
    pointer_to_tab_1 := pointer_to_tab_1^.lower;
  end;

  Close(f);
  {$I+}
  if ioresult <> 0 then
    error_message;
end;

function edit_path: boolean;
begin
  edit_path := False;
  pass_field;
  if (path[1] = #1) then
  begin
    path := 'C:\';
  end
  else
  begin
    if not (Copy(path, Length(path) - 5, 6) = '.appdb') then
      path := path + '.appdb';
    edit_path := True;
  end;
end;

end.
