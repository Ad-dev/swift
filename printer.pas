unit printer;

interface

uses
  crt,
  values, 
  control,
  Windows,
  structure;

procedure manual;  
procedure pass_field;
procedure print_page;
procedure print_menu;
procedure end_preview;
procedure print_table;
procedure view_message;
procedure error_message;
procedure print_records;
procedure prepare_window;
procedure choice(trigger: boolean);
procedure show_position(is_it_target, is_it_editing: boolean);
procedure update_buttons(position: byte; sub, confirm, trigger: boolean);

implementation

procedure prepare_window;
var
  console_handler: HWND;
  hm: HMENU;
  frame: ^RECT;
begin
  Window(1, 1, 75, 26);
  new(frame);
  console_handler := GetConsoleWindow;
  hm := GetSystemMenu(console_handler, False);
  EnableMenuItem(hm, SC_CLOSE, MF_GRAYED);
  RemoveMenu(hm, SC_SIZE, MF_BYCOMMAND);
  RemoveMenu(hm, SC_MAXIMIZE, MF_BYCOMMAND);
  GetWindowRect
  (
    console_handler,
    frame
    );
  frame^.Right := WINDOW_WIDTH;
  frame^.Bottom := WINDOW_HEIGHT;
  MoveWindow
  (
    console_handler,
    frame^.Left,
    frame^.Top,
    frame^.Right,
    frame^.Bottom,
    True
    );
  SetConsoleTitle(WINDOW_TITLE);
  SetConsoleCP(1251);
  DrawMenuBar(console_handler);
  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_BACKGROUND);
  cursoroff;
  ClrScr;
  Dispose(frame);
end;

procedure print_page;
var
  tmp: ShortString;
begin
  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_FRAME);
  GotoXY(64, 26);
  str(current_page, tmp);
  Write(('Page: ' + tmp): 11);
  TextBackground(WINDOW_BACKGROUND);
end;

procedure show_position(is_it_target, is_it_editing: boolean);
var
  tmp: ShortString;
begin
  if (is_it_target) then
  begin
    TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
    TextBackground(WINDOW_POINTER);
  end
  else
  begin
    if (current_position_y mod 2 = 0) then
      TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT)
    else
      TextColor(WINDOW_FORGEGROUND);
    TextBackground(WINDOW_BACKGROUND);
  end;
  if (current_table = 0) then
  begin
    case current_position_x of
      1:
      begin
        GotoXY(2, 2 + current_position_y);
        if (is_it_editing) then
        begin
          Write('': 23);
          GotoXY(2, 2 + current_position_y);
          Write('~input');
        end
        else
        begin
          if (Length(pointer_to_tab_1^.program_name) <= 23) then
            Write(pointer_to_tab_1^.program_name: 23)
          else
          begin
            tmp := pointer_to_tab_1^.program_name;
            SetLength(tmp, 20);
            Write(tmp, '...');
          end;
        end;
      end;
      2:
      begin
        GotoXY(26, 2 + current_position_y);
        if (is_it_editing) then
        begin
          Write('': 9);
          GotoXY(26, 2 + current_position_y);
          Write('~input');
        end
        else
        begin
          if (pointer_to_tab_1^.size <= 999999999) then
            Write(pointer_to_tab_1^.size: 9)
          else
          begin
            str(pointer_to_tab_1^.size, tmp);
            SetLength(tmp, 6);
            Write(tmp, '...');
          end;
        end;
      end;
      3:
      begin
        GotoXY(36, 2 + current_position_y);
        if (is_it_editing) then
        begin
          Write('': 8);
          GotoXY(36, 2 + current_position_y);
          Write('~input');
        end
        else
        begin
          if (pointer_to_tab_1^.cost <= 99999999) then
            Write(pointer_to_tab_1^.cost: 8)
          else
          begin
            str(pointer_to_tab_1^.cost, tmp);
            SetLength(tmp, 5);
            Write(tmp, '...');
          end;
        end;
      end;
      4:
      begin
        GotoXY(45, 2 + current_position_y);
        if (is_it_editing) then
        begin
          Write('': 23);
          GotoXY(45, 2 + current_position_y);
          Write('~input');
        end
        else
        begin
          if (Length(pointer_to_tab_1^.developer^) <= 23) then
            Write(pointer_to_tab_1^.developer^: 23)
          else
          begin
            tmp := pointer_to_tab_1^.developer^;
            SetLength(tmp, 20);
            Write(tmp, '...');
          end;
        end;
      end;
      5:
      begin
        GotoXY(69, 2 + current_position_y);
        if (is_it_editing) then
          Write('~input')
        else
          Write((pointer_to_tab_1^.rating + ' star'));
      end;
    end;
  end
  else
  begin
    if (current_position_x > 3) then
      current_position_x := 3;
    case current_position_x of
      1:
      begin
        GotoXY(2, 2 + current_position_y);
        if (is_it_editing) then
        begin
          Write('': 25);
          GotoXY(2, 2 + current_position_y);
          Write('~input');
        end
        else
        begin
          if (Length(pointer_to_tab_2^.developer^) <= 25) then
            Write(pointer_to_tab_2^.developer^: 25)
          else
          begin
            tmp := pointer_to_tab_2^.developer^;
            SetLength(tmp, 22);
            Write(tmp, '...');
          end;
        end;
      end;
      2:
      begin
        GotoXY(28, 2 + current_position_y);
        if (is_it_editing) then
        begin
          Write('': 25);
          GotoXY(28, 2 + current_position_y);
          Write('~input');
        end
        else
        begin
          if (Length(pointer_to_tab_2^.description) <= 25) then
            Write(pointer_to_tab_2^.description: 25)
          else
          begin
            tmp := pointer_to_tab_2^.description;
            SetLength(tmp, 22);
            Write(tmp, '...');
          end;
        end;
      end;
      3:
      begin
        GotoXY(54, 2 + current_position_y);
        if (is_it_editing) then
        begin
          Write('': 21);
          GotoXY(54, 2 + current_position_y);
          Write('~input');
        end
        else
        begin
          if (Length(pointer_to_tab_2^.e_mail) <= 21) then
            Write(pointer_to_tab_2^.e_mail: 21)
          else
          begin
            tmp := pointer_to_tab_2^.e_mail;
            SetLength(tmp, 18);
            Write(tmp, '...');
          end;
        end;
      end;
    end;
  end;
  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_BACKGROUND);
end;

procedure print_table;
var
  iterator: byte;
begin
  TextBackground(WINDOW_BACKGROUND);
  TextColor(WINDOW_FORGEGROUND);
  ClrScr;

  TextBackground(WINDOW_FRAME);
  GotoXY(1, 2);
  ClrEol;
  TextBackground(WINDOW_BACKGROUND);

  TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
  for iterator := 2 to 12 do
  begin
    GotoXY(1, iterator * 2);
    ClrEol;
  end;

  TextBackground(WINDOW_FRAME);
  TextColor(WINDOW_FORGEGROUND);

  if (current_table = 0) then
  begin
    GotoXY(2, 2);
    Write('Program  Name': 23 div 2 + 7);
    GotoXY(26, 2);
    Write(('Size (' + SIZE_UNIT + ')'): 9);
    GotoXY(36, 2);
    Write(('Cost (' + CURRENCY_UNIT + ')'): 8);
    GotoXY(45, 2);
    Write('Developer': 23 div 2 + 5);
    GotoXY(69, 2);
    Write('Rating': 6);

    for iterator := 3 to 25 do
    begin
      GotoXY(1, iterator);
      Write(#32);
      GotoXY(25, iterator);
      Write(#32);
      GotoXY(35, iterator);
      Write(#32);
      GotoXY(44, iterator);
      Write(#32);
      GotoXY(68, iterator);
      Write(#32);
      GotoXY(75, iterator);
      Write(#32);
    end;
  end
  else
  begin
    GotoXY(2, 2);
    Write('Developer': 25 div 2 + 5);
    GotoXY(28, 2);
    Write('Description': 25 div 2 + 6);
    GotoXY(54, 2);
    Write('E-mail': 22 div 2 + 3);

    for iterator := 3 to 25 do
    begin
      GotoXY(1, iterator);
      Write(#32);
      GotoXY(27, iterator);
      Write(#32);
      GotoXY(53, iterator);
      Write(#32);
      GotoXY(75, iterator);
      Write(#32);
    end;
  end;

  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_FRAME);
  GotoXY(1, 26);
  ClrEol;
  TextBackground(WINDOW_BACKGROUND);
  print_page;
  show_position(True, False);
  if (view_mode) then
    view_message;
end;

procedure print_records;
var
  iterator, jeterator: QWord;
  old_x, old_y: byte;
  old_view_node: view_list;
  node_1, old_record_1: line_tab_1;
  node_2, old_record_2: line_tab_2;
begin
  old_x := current_position_x;
  old_y := current_position_y;
  current_position_x := 1;
  current_position_y := 1;
  if (not view_mode) then
  begin
    old_record_1 := pointer_to_tab_1;
    old_record_2 := pointer_to_tab_2;
    node_1 := list_peak_1;
    node_2 := list_peak_2;
    print_table;
    if (current_table = 0) then
    begin
      for iterator := 1 to ((current_page - 1) * 23) do
      begin
        if (node_1^.lower <> nil) then
          node_1 := node_1^.lower;
      end;
      for iterator := 1 to 23 do
      begin
        if (node_1 <> nil) then
        begin
          pointer_to_tab_1 := node_1;
          current_position_y := iterator;
          for jeterator := 1 to 5 do
          begin
            current_position_x := jeterator;
            show_position(False, False);
          end;
          node_1 := node_1^.lower;
        end;
      end;
    end
    else
    begin
      for iterator := 1 to ((current_page - 1) * 23) do
      begin
        if (node_2^.lower <> nil) then
          node_2 := node_2^.lower;
      end;
      for iterator := 1 to 23 do
      begin
        if (node_2 <> nil) then
        begin
          pointer_to_tab_2 := node_2;
          current_position_y := iterator;
          for jeterator := 1 to 3 do
          begin
            current_position_x := jeterator;
            show_position(False, False);
          end;
          node_2 := node_2^.lower;
        end;
      end;
    end;
    pointer_to_tab_1 := old_record_1;
    pointer_to_tab_2 := old_record_2;
  end
  else
  begin
    old_view_node := view_node;
    view_node := view_head;
    print_table;
    for iterator := 1 to ((current_page - 1) * 23) do
    begin
      if (view_node^.lower <> nil) then
        view_node := view_node^.lower;
    end;
    for iterator := 1 to 23 do
    begin
      if (view_node <> nil) then
      begin
        pointer_to_tab_1 := view_node^.to_firs_tab;
        current_position_y := iterator;
        for jeterator := 1 to 5 do
        begin
          current_position_x := jeterator;
          show_position(False, False);
        end;
        view_node := view_node^.lower;
      end;
    end;
    view_node := old_view_node;
    pointer_to_tab_1 := view_node^.to_firs_tab;
  end;
  current_position_x := old_x;
  current_position_y := old_y;
  show_position(True, False);
end;

procedure error_message;
begin
  GotoXY(1, 1);
  TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
  TextBackground(WINDOW_ERROR);
  ClrEol;
  GotoXY(1, 1);
  Write('Error': 75 div 2 + 3);
  ReadKey;
  GotoXY(1, 1);
  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_BACKGROUND);
  ClrEol;
end;

procedure view_message;
begin
  GotoXY(1, 1);
  TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
  TextBackground(WINDOW_POINTER);
  ClrEol;
  GotoXY(1, 1);
  if (search) then
    Write('Search mod': 75 div 2 + 5);
  if (sort) then
    Write('Sort mod': 75 div 2 + 4);
  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_BACKGROUND);
end;

procedure end_preview;
begin
  TextBackground(WINDOW_FRAME);
  ClrScr;
  GotoXY((75 div 2) - 29 div 2, 26 div 2);
  WriteLn('process terminated successful': 29);
  ReadKey;
  TextBackground(0);
  TextColor(10);
  ClrScr;
end;

procedure print_menu;
var
  i: byte;
begin
  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_FRAME);
  ClrScr;

  TextBackground(WINDOW_FORGEGROUND);
  GotoXY(23, 2);
  Write('    ');
  GotoXY(36, 2);
  Write('    ');
  GotoXY(47, 2);
  Write(' ');

  GotoXY(22, 3);
  Write(' ');
  GotoXY(26, 3);
  Write(' ');
  GotoXY(28, 3);
  Write('   ');
  GotoXY(32, 3);
  Write('   ');
  GotoXY(36, 3);
  Write(' ');
  GotoXY(40, 3);
  Write(' ');
  GotoXY(43, 3);
  Write('  ');
  GotoXY(46, 3);
  Write('   ');
  GotoXY(51, 3);
  Write('  ');

  GotoXY(22, 4);
  Write('     ');
  GotoXY(28, 4);
  Write(' ');
  GotoXY(30, 4);
  Write(' ');
  GotoXY(32, 4);
  Write(' ');
  GotoXY(34, 4);
  Write(' ');
  GotoXY(36, 4);
  Write(' ');
  GotoXY(40, 4);
  Write(' ');
  GotoXY(42, 4);
  Write(' ');
  GotoXY(44, 4);
  Write(' ');
  GotoXY(47, 4);
  Write(' ');
  GotoXY(50, 4);
  Write(' ');
  GotoXY(52, 4);
  Write(' ');

  GotoXY(22, 5);
  Write(' ');
  GotoXY(26, 5);
  Write(' ');
  GotoXY(28, 5);
  Write('   ');
  GotoXY(32, 5);
  Write('   ');
  GotoXY(36, 5);
  Write('    ');
  GotoXY(42, 5);
  Write('   ');
  GotoXY(47, 5);
  Write('  ');
  GotoXY(50, 5);
  Write('   ');

  GotoXY(28, 6);
  Write(' ');
  GotoXY(32, 6);
  Write(' ');

  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_BACKGROUND);
  for i := 7 to 10 do
  begin
    GotoXY(19, i);
    Write('                                     ');
    GotoXY(19, i + 5);
    Write('                                     ');
    GotoXY(19, i + 10);
    Write('                                     ');
    GotoXY(19, i + 15);
    Write('                                     ');
  end;

  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_BACKGROUND);
end;

procedure update_buttons(position: byte; sub, confirm, trigger: boolean);
var
  i: byte;
begin
  TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
  TextBackground(WINDOW_BACKGROUND);
  for i := 7 to 9 do
  begin
    GotoXY(20, i);
    Write('                                   ');
    GotoXY(20, i + 5);
    Write('                                   ');
    GotoXY(20, i + 10);
    Write('                                   ');
    GotoXY(20, i + 15);
    Write('                                   ');
  end;
  if (not sub) then
  begin
    case position of
      1:
      begin
        if (confirm) then
        begin
          GotoXY(19, 7);
          choice(trigger);
        end
        else
        begin
          TextBackground(WINDOW_POINTER);
          TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
          GotoXY(20, 7);
          Write('                                   ');
          GotoXY(20, 8);
          Write('                                   ');
          GotoXY(20, 8);
          Write('New': 35 div 2 + 2);
          GotoXY(20, 9);
          Write('                                   ');
        end;
        TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
        TextBackground(WINDOW_BACKGROUND);
        GotoXY(20, 13);
        Write('Load': 35 div 2 + 2);
        GotoXY(20, 18);
        Write('Manual': 35 div 2 + 3);
        GotoXY(20, 23);
        Write('Exit': 35 div 2 + 2);
      end;
      2:
      begin
        GotoXY(20, 8);
        Write('New': 35 div 2 + 2);
        if (confirm) then
        begin
          GotoXY(19, 12);
          choice(trigger);
        end
        else
        begin
          TextBackground(WINDOW_POINTER);
          TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
          GotoXY(20, 12);
          Write('                                   ');
          GotoXY(20, 13);
          Write('                                   ');
          GotoXY(20, 13);
          Write('Load': 35 div 2 + 2);
          GotoXY(20, 14);
          Write('                                   ');
        end;
        TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
        TextBackground(WINDOW_BACKGROUND);
        GotoXY(20, 18);
        Write('Manual': 35 div 2 + 3);
        GotoXY(20, 23);
        Write('Exit': 35 div 2 + 2);
      end;
      3:
      begin
        GotoXY(20, 8);
        Write('New': 35 div 2 + 2);
        GotoXY(20, 13);
        Write('Load': 35 div 2 + 2);
        TextBackground(WINDOW_POINTER);
        TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
        GotoXY(20, 17);
        Write('                                   ');
        GotoXY(20, 18);
        Write('                                   ');
        GotoXY(20, 18);
        Write('Manual': 35 div 2 + 3);
        GotoXY(20, 19);
        Write('                                   ');
        TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
        TextBackground(WINDOW_BACKGROUND);
        GotoXY(20, 23);
        Write('Exit': 35 div 2 + 2);
      end;
      4:
      begin
        GotoXY(20, 8);
        Write('New': 35 div 2 + 2);
        GotoXY(20, 13);
        Write('Load': 35 div 2 + 2);
        GotoXY(20, 18);
        Write('Manual': 35 div 2 + 3);
        if (confirm) then
        begin
          GotoXY(19, 22);
          choice(trigger);
        end
        else
        begin
          TextBackground(WINDOW_POINTER);
          TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
          GotoXY(20, 22);
          Write('                                   ');
          GotoXY(20, 23);
          Write('                                   ');
          GotoXY(20, 23);
          Write('Exit': 35 div 2 + 2);
          GotoXY(20, 24);
          Write('                                   ');
        end;
        TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
        TextBackground(WINDOW_BACKGROUND);
      end;
    end;
  end
  else
  begin
    case position of
      1:
      begin
        TextBackground(WINDOW_POINTER);
        TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
        GotoXY(20, 7);
        Write('                                   ');
        GotoXY(20, 8);
        Write('                                   ');
        GotoXY(20, 8);
        Write('Return': 35 div 2 + 3);
        GotoXY(20, 9);
        Write('                                   ');
        TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
        TextBackground(WINDOW_BACKGROUND);
        GotoXY(20, 13);
        Write('Save': 35 div 2 + 2);
        GotoXY(20, 18);
        Write('Manual': 35 div 2 + 3);
        GotoXY(20, 23);
        Write('Detach': 35 div 2 + 3);
      end;
      2:
      begin
        GotoXY(20, 8);
        Write('Return': 35 div 2 + 3);
        if (confirm) then
        begin
          GotoXY(19, 12);
          choice(trigger);
        end
        else
        begin
          TextBackground(WINDOW_POINTER);
          TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
          GotoXY(20, 12);
          Write('                                   ');
          GotoXY(20, 13);
          Write('                                   ');
          GotoXY(20, 13);
          Write('Save': 35 div 2 + 2);
          GotoXY(20, 14);
          Write('                                   ');
        end;
        TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
        TextBackground(WINDOW_BACKGROUND);
        GotoXY(20, 18);
        Write('Manual': 35 div 2 + 3);
        GotoXY(20, 23);
        Write('Detach': 35 div 2 + 3);
      end;
      3:
      begin
        GotoXY(20, 8);
        Write('Return': 35 div 2 + 3);
        GotoXY(20, 13);
        Write('Save': 35 div 2 + 2);
        TextBackground(WINDOW_POINTER);
        TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
        GotoXY(20, 17);
        Write('                                   ');
        GotoXY(20, 18);
        Write('                                   ');
        GotoXY(20, 18);
        Write('Manual': 35 div 2 + 3);
        GotoXY(20, 19);
        Write('                                   ');
        TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
        TextBackground(WINDOW_BACKGROUND);
        GotoXY(20, 23);
        Write('Detach': 35 div 2 + 3);
      end;
      4:
      begin
        GotoXY(20, 8);
        Write('Return': 35 div 2 + 3);
        GotoXY(20, 13);
        Write('Save': 35 div 2 + 2);
        GotoXY(20, 18);
        Write('Manual': 35 div 2 + 3);
        if (confirm) then
        begin
          GotoXY(19, 22);
          choice(trigger);
        end
        else
        begin
          TextBackground(WINDOW_POINTER);
          TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
          GotoXY(20, 22);
          Write('                                   ');
          GotoXY(20, 23);
          Write('                                   ');
          GotoXY(20, 23);
          Write('Detach': 35 div 2 + 3);
          GotoXY(20, 24);
          Write('                                   ');
        end;
        TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
        TextBackground(WINDOW_BACKGROUND);
      end;
    end;
  end;
  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_BACKGROUND);
end;

procedure choice(trigger: boolean);
begin
  if (trigger) then
  begin
    GotoXY(20, WhereY);
    TextBackground(WINDOW_POINTER);
    TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
    Write('                 ');
    TextBackground(WINDOW_BACKGROUND);
    TextColor(WINDOW_FORGEGROUND);
    Write(' ');
    GotoXY(20, WhereY + 1);
    TextBackground(WINDOW_POINTER);
    TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
    Write('                 ');
    GotoXY(20, WhereY);
    Write('Yes': 9);
    TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
    TextBackground(WINDOW_BACKGROUND);
    GotoXY(38, WhereY);
    Write('No': 9);
    TextBackground(WINDOW_POINTER);
    TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
    GotoXY(37, WhereY);
    TextBackground(WINDOW_BACKGROUND);
    TextColor(WINDOW_FORGEGROUND);
    Write(' ');
    GotoXY(20, WhereY + 1);
    TextBackground(WINDOW_POINTER);
    TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
    Write('                 ');
    TextBackground(WINDOW_BACKGROUND);
    TextColor(WINDOW_FORGEGROUND);
    Write(' ');
  end
  else
  begin
    GotoXY(37, WhereY);
    TextBackground(WINDOW_BACKGROUND);
    TextColor(WINDOW_FORGEGROUND);
    Write(' ');
    TextBackground(WINDOW_POINTER);
    TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
    Write('                 ');
    GotoXY(37, WhereY + 1);
    TextBackground(WINDOW_BACKGROUND);
    TextColor(WINDOW_FORGEGROUND);
    Write(' ');
    TextBackground(WINDOW_POINTER);
    TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
    Write('                 ');
    GotoXY(38, WhereY);
    Write('No': 9);
    TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
    TextBackground(WINDOW_BACKGROUND);
    GotoXY(20, WhereY);
    Write('Yes': 9);
    TextBackground(WINDOW_POINTER);
    TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
    GotoXY(37, WhereY + 1);
    TextBackground(WINDOW_BACKGROUND);
    TextColor(WINDOW_FORGEGROUND);
    Write(' ');
    TextBackground(WINDOW_POINTER);
    TextColor(WINDOW_FORGEGROUND + WINDOW_GLUT_BIT);
    Write('                 ');
  end;
  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_BACKGROUND);
end;

procedure manual;
begin
  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_FRAME);
  ClrScr;
  GotoXY(75 div 2 - 6, 26);
  Write('Press any key!');
  Window(2,2,74,25);
  TextBackground(WINDOW_BACKGROUND);
  ClrScr;
  WriteLn('table:');
  WriteLn('enter - edit cell');         
  WriteLn('ctrl + delete - delete record');
  WriteLn('ctrl + + (on table 1) - add record');
  WriteLn('+ - add sub record');
  WriteLn('arrow up - offset up');
  WriteLn('arrow down - offset down');
  WriteLn('arrow right - offset right');
  WriteLn('arrow left - offset left');
  WriteLn('ctrl + arrow right - swap to first table');
  WriteLn('ctrl + arrow left - swap to second table');
  WriteLn('esc - back/exit');
  WriteLn('F1 (chose devekoper from table 1) - search mod');
  WriteLn('F2 (on table 2)- sort mod');

  WriteLn;
  WriteLn('editor:');
  WriteLn('ctrl + backspace - clear string');
  WriteLn('backspace - delete last char');
  WriteLn('enter - save changes');

  WriteLn;
  WriteLn('sort mod:');             
  WriteLn('ctrl + enter - save sort');

  ReadKey;               
  Window(1,1,75,26);
  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_BACKGROUND);
end;

procedure pass_field;
begin
  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_FRAME);
  ClrScr;                       
  GotoXY(75 div 2 - 10, 1);
  Write('Enter the file path:');
  Window(2,2,74,25);
  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_BACKGROUND);
  ClrScr;
  
  input_path;

  Window(1,1,75,26);
  TextColor(WINDOW_FORGEGROUND);
  TextBackground(WINDOW_BACKGROUND);
end;

end.


