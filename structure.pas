unit structure;

interface

type
  view_list = ^ptr_view_list;
  line_tab_1 = ^ptr_line_tab_1;
  line_tab_2 = ^ptr_line_tab_2;
  bound_cells = string[74];
  stars = '0'..'5';

  ptr_line_tab_1 = record
    program_name: string[74];
    size: cardinal;
    cost: cardinal;
    developer: ^bound_cells;
    rating: stars;
    upper, lower: line_tab_1;
    dependency_code: QWord;
  end;

  save_record_1 = record
    program_name: string[74];
    size: cardinal;
    cost: cardinal;
    rating: stars;
    dependency_code: QWord;
  end;

  ptr_line_tab_2 = record
    developer: ^bound_cells;
    description: string[74];
    e_mail: string[74];
    upper, lower: line_tab_2;
    num_code: QWord;
  end;

  save_record_2 = record
    developer: string[74];
    description: string[74];
    e_mail: string[74];
  end;

  ptr_view_list = record
    to_firs_tab: line_tab_1;
    lower, upper: view_list;
  end;

var
  path: string;
  current_page, max_code: QWord;
  view_head, view_node: view_list;             
  view_mode, search, sort: boolean;
  list_peak_1, pointer_to_tab_1: line_tab_1;
  list_peak_2, pointer_to_tab_2: line_tab_2;
  current_position_x, current_position_y, cpx_old, cpy_old, current_table: byte;

implementation

end.
