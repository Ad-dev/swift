unit values;

interface

const
  WINDOW_TITLE = 'AppData';
  WINDOW_WIDTH = 634;
  WINDOW_HEIGHT = 456;
  WINDOW_FRAME = $3;
  WINDOW_ERROR = $4;
  WINDOW_POINTER = $2;
  WINDOW_BACKGROUND = $7;
  WINDOW_FORGEGROUND = $0;
  WINDOW_GLUT_BIT = $80;

  CURRENCY_UNIT = '$';
  SIZE_UNIT = 'Mb';

implementation

end.
