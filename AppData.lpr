program AppData;

{$AppType Console}
{$mode objfpc}
{$H+}
{******************************************************************************
 *                                                                            *
 *  Program: AppData                                                          *
 *  Destination: Information system of applications                           *
 *  Build: 2.1.7                                                              *
 *                                                                            *
 *  Developer: Bukina A.S.                                                    *
 *  Created: September 16, 2018                                               *
 *  Last modification: October 27, 2018                                       *
 *                                                                            *
 ******************************************************************************}

uses
  crt,
  values,
  control,
  printer,
  manager,
  structure;

  procedure main_loop;
  var
    loop: boolean;
  begin
    loop := True;
    while (loop) do
    begin
      case ReadKey of
        #0:
        begin
          case ReadKey of
            #77://right
            begin
              if (not view_mode) then
                swap_position(1)
              else
                swap_view_position(1);
            end;
            #80://down
            begin
              if (not view_mode) then
                swap_position(2)
              else
                swap_view_position(2);
            end;
            #75://left
            begin
              if (not view_mode) then
                swap_position(3)
              else
                swap_view_position(3);
            end;
            #72://up
            begin
              if (not view_mode) then
                swap_position(4)
              else
                swap_view_position(4);
            end;
            #78://ctrl + +
            begin
              if (current_table = 1) then
              begin
                add_record;
                print_records;
              end
              else
                boop(1);
            end;
            #115://ctrl + left
            begin
              if ((current_table = 0) and (not view_mode)) then
              begin
                current_table := 1;
                current_page := 1;
                current_position_x := 1;
                current_position_y := 1;
                pointer_to_tab_2 := list_peak_2;
                print_records;
              end
              else
                boop(1);
            end;
            #116://ctrl + right
            begin
              if ((current_table = 1) and (not view_mode)) then
              begin
                current_table := 0;
                current_page := 1;
                current_position_x := 1;
                current_position_y := 1;
                pointer_to_tab_1 := list_peak_1;
                print_records;
              end
              else
                boop(1);
            end;
            #147://ctrl + del
            begin
              if (current_table = 1) then
                annihilation
              else
              begin
                elimination;
                print_records;
              end;
            end;
            #59://F1
            begin
              if (current_table = 1) then
                searcher
              else
              begin
                if (search) then
                  disappear
                else
                  boop(1);
              end;
            end;
            #60://F2
            begin
              if ((current_table = 0) and (not search) and (not sort)) then
                sorter
              else
              begin
                if (sort) then
                  disappear
                else
                  boop(1);
              end;
            end;
          end;
        end;
        #27://Esc
        begin
          if ((not search) and (not sort)) then
            loop := False
          else
            disappear;
        end;
        #10://ctrl + enter
        begin
          if (sort) then
            confirm_sort;
        end;
        #13://enter
        begin
          if (not sort) then
            edit_cell
          else
            boop(0);
        end;
        #43://+
        begin
          if (current_table = 1) then
          begin
            add_sub_record;
            current_table := 0;
            current_position_x := 1;
            current_position_y := 1;
            pointer_to_tab_1 := list_peak_1;
            while (pointer_to_tab_1^.lower <> nil) do
            begin
              pointer_to_tab_1 := pointer_to_tab_1^.lower;
              Inc(current_position_y);
              if (current_position_y > 23) then
              begin
                current_position_y := 1;
                Inc(current_page);
              end;
            end;
            print_records;
          end
          else
            boop(1);
        end;
      end;
    end;
  end;

  procedure main_menu;
  var
    position: byte;
    confirm, loop, trigger, sub: boolean;
  begin
    position := 1;
    confirm := False;
    trigger := False;
    sub := False;
    loop := True;
    print_menu;
    update_buttons(position, sub, confirm, trigger);
    while (loop) do
    begin
      case ReadKey of
        #0:
        begin
          case ReadKey of
            #77://right
            begin
              if (confirm) then
                if (trigger) then
                begin
                  trigger := False;
                  update_buttons(position, sub, confirm, trigger);
                end;
            end;
            #80://down
            begin
              if (not confirm) then
              begin
                Inc(position);
                if (position = 5) then
                  position := 1;
                update_buttons(position, sub, confirm, trigger);
              end;
            end;
            #75://left
            begin
              if (confirm) then
                if (not trigger) then
                begin
                  trigger := True;
                  update_buttons(position, sub, confirm, trigger);
                end;
            end;
            #72://up
            begin
              if (not confirm) then
              begin
                Dec(position);
                if (position = 0) then
                  position := 4;
                update_buttons(position, sub, confirm, trigger);
              end;
            end;
          end;
        end;
        #13://enter
        begin
          if (not confirm) then
          begin
            case position of
              1:
              begin
                if (sub) then
                begin
                  pointer_to_tab_1 := list_peak_1;
                  pointer_to_tab_2 := list_peak_2;
                  current_page := 1;
                  current_position_x := 1;
                  current_position_y := 1;
                  print_records;
                  main_loop;
                  print_menu;
                  update_buttons(position, sub, confirm, trigger);
                end
                else
                begin
                  confirm := True;
                  update_buttons(position, sub, confirm, trigger);
                end;
              end;
              3:
              begin
                manual;
                confirm := False;
                trigger := False;
                print_menu;
                update_buttons(position, sub, confirm, trigger);
              end;
              else
              begin
                confirm := True;
                update_buttons(position, sub, confirm, trigger);
              end;
            end;
          end
          else
          begin
            if (trigger) then
            begin
              case position of
                1:
                begin
                  if (not sub) then
                  begin
                    outset;
                    sub := True;
                    main_loop;
                  end
                  else
                  begin
                    current_page := 1;
                    current_position_x := 1;
                    current_position_y := 1;
                    main_loop;
                  end;
                  confirm := False;
                  trigger := False;
                  print_menu;
                  update_buttons(position, sub, confirm, trigger);
                end;
                2:
                begin
                  if (edit_path) then
                  begin
                  if (not sub) then
                  begin
                    if (loader) then
                    begin
                      sub := True;
                      main_loop;
                    end;
                  end
                  else
                    saver;
                  end;
                  confirm := False;
                  trigger := False;
                  print_menu;
                  update_buttons(position, sub, confirm, trigger);
                end;
                4:
                begin
                  if (not sub) then
                  begin
                    loop := False;
                    end_preview;
                  end
                  else
                  begin
                    crash_table;
                    confirm := False;
                    trigger := False;
                    sub := False;
                    print_menu;
                    update_buttons(position, sub, confirm, trigger);
                  end;
                end;
              end;
            end
            else
            begin
              confirm := False;
              trigger := False;
              update_buttons(position, sub, confirm, trigger);
            end;
          end;
        end;
      end;
    end;
  end;

begin
  path := 'C:\';
  prepare_window;
  main_menu;
end.
